package pl.me.easyrulestest;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import starts.Start;

public class MainActivity extends Activity {

    private Button button;
    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
//        editText = (EditText) findViewById(R.id.editText);
//        System.out.println("DEBUG: "+ Environment.getDataDirectory().getAbsolutePath()); // /data
//        System.out.println("DEBUG: " + Environment.getExternalStorageDirectory().getAbsolutePath());// /storage/emulated/0

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String input = editText.getText().toString();
//                HelloWorldRuleAnnot helloWorldRule = new HelloWorldRuleAnnot();
//                helloWorldRule.setInput(input.trim());
//                helloWorldRule.setTextView(textView);
//                RulesEngine rulesEngine = aNewRulesEngine().build();
//                rulesEngine.registerRule(helloWorldRule);
//                rulesEngine.fireRules();
//            }
//        });

//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Person tom = new Person("Tom", 14);
//                textView.setText(textView.getText().toString() + "\nTom: Hi! can I have some Vodka please?");
//                RulesEngine re = aNewRulesEngine().named("shop rules engine").withSkipOnFirstFailedRule
//                        (true).withSkipOnFirstAppliedRule(true).build();
//                //register rules
//                re.registerRule(new AgeRule(tom, textView));
//                re.registerRule(new AlcoholRule(tom, textView));
//                //fire rules
//                re.fireRules();
//            }
//        });

//        button.setOnClickListener(new View.OnClickListener() {
//            public static final int EVERY_SECOND = 1;
//
//            @Override
//            public void onClick(View v) {
//                RulesEngineScheduler scheduler;
//                RulesEngine rulesEngine = RulesEngineBuilder
//                        .aNewRulesEngine()
//                        .named("time rules engine")
//                        .withSilentMode(true)
//                        .build();
//
//                TimeRule timeRule = new TimeRule();
//                rulesEngine.registerRule(timeRule);
//
//                try {
//                    scheduler = RulesEngineScheduler.getInstance();
//                    scheduler.start();
//                    scheduler.scheduleAtWithInterval(rulesEngine,
//                            new Date(),
//                            EVERY_SECOND);
//                } catch (RulesEngineSchedulerException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
        new Monitor().execute();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
                            .getAbsolutePath()+"/benchmark/easyrules.txt",true));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                start = new Date();
                Start a=new Start();
                a._ocenakandydata.setOcenaRozmowy(2.0);
                a._ocenakandydata.setOcenaKwalifikacji(5.0);
                a._ocenakandydata.setOcenaTestow(2.0);
                a.rulesEngine.fireRules();
                System.out.println(a.printState());
                ( (TextView) findViewById(R.id.textView)).setText(a.printState());
                end = new Date();
                ifContinue = false;
            }
        });
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
