package pl.me.easyrulestest;

import android.widget.TextView;

import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;


/**
 * Created by Piotr1 on 2016-04-28.
 */
@Rule(name = "Hello World rule A",
        description = "Say Hello to duke's friends only")
public class HelloWorldRuleAnnot {
    /**
     * The user input which represents the data
     * that the rule will operate on.
     */
    private String input;
    private TextView textView;

    @Condition
    public boolean checkInput() {
        //The rule should be applied only if
        //the user's response is yes (duke friend)
        return input.equalsIgnoreCase("yes");
    }

    @Action
    public void sayHelloToDukeFriend() throws Exception {
        //When rule conditions are satisfied,
        //prints 'Hello duke's friend!' to the console
        //System.out.println("Hello duke's friend!");
        textView.setText("Hello duke's friend!");
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
