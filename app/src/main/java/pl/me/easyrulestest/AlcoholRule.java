package pl.me.easyrulestest;

import android.widget.TextView;

import org.easyrules.core.BasicRule;

/**
 * Created by Piotr1 on 2016-05-04.
 */
public class AlcoholRule extends BasicRule {
    private Person person;
    TextView textView;

    public AlcoholRule(Person person, TextView textView) {
        super("AlcoholRule",
                "Children are not allowed to buy alcohol",
                2);
        this.person = person;
        this.textView = textView;
    }

    @Override
    public boolean evaluate() {
        return !person.isAdult();
    }

    @Override
    public void execute(){
        System.out.println("Shop: Sorry "+person.getName()+", you are not allowed to buy alcohol");
        textView.setText(textView.getText().toString()+"Shop: Sorry "+person.getName()+", you are not allowed to buy alcohol");
    }
}
