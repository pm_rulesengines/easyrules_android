package pl.me.easyrulestest;

import android.widget.TextView;

import org.easyrules.core.BasicRule;

/**
 * Created by Piotr1 on 2016-05-04.
 */
public class AgeRule extends BasicRule {
    private static final int ADULT_AGE = 18;

    private Person person;
    TextView textView;

    public AgeRule(Person person, TextView textView) {
                                                                                // 1 - rule priority
        super("AgeRule", "Check if person's age is > 18 and marks the person as adult", 1);
        this.person = person;
        this.textView = textView;
    }

    @Override
    public boolean evaluate() {
        return person.getAge() > ADULT_AGE;
    }

    @Override
    public void execute() {
        person.setAdult(true);
        System.out.printf("Person %s has been marked as adult", person.getName());
        textView.setText(textView.getText().toString()+"\nPerson "+person.getName()+" has been marked as " +
                "adult");
    }
}
