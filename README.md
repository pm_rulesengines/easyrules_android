# README #

This README contains specific information about EasyRules rules engine.

### Warning ###

No additional files are required by EasyRules. All data and rules are stored as Java classes. However, a problem with loading dependencies from Maven repository may occur. If you get errors saying that Gradle can't find easyrules modules, try to remove these dependencies from the Gradle script, download JAR files with required libraries (http://central.maven.org/maven2/org/easyrules/easyrules-core/2.5.0/easyrules-core-2.5.0.jar) and add them via "Project Structure -> Dependencies -> Jar dependencies" option in Android Studio.